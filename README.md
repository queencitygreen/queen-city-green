Our mission is to lead the industry in bringing high-quality hemp cannabidiol (CBD) products to the everyday active adult. We aim to educate the world on the benefits of hemp extract and how CBD can help. 

Our goal? To offer the industry's highest quality, most trusted products at fair prices and help more active people Recover like Royalty.
Comfortable Workplace
When you buy hemp CBD oil from us, you know youre getting products you can trust. No one makes CBD-rich hemp oil products like Queen City Green.

Our hemp CBD is non-GMO and contains no pesticides, solvents, herbicides or chemical fertilizers. We have all our products tested by third-party laboratories so you can buy CBD hemp oil that is completely safe for consumption. 

We are headquartered in Charlotte, NC and our process from seed to sale is 100% USA-made and manufactured.

There is no question where to buy CBD oil from that you can trust - the answer is Queen City Green.

Website: https://queencitygreen.com/
